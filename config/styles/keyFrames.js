import { keyframes } from 'styled-components'

export const fadeIn = keyframes`
  from{
    opacity: 0;
  }
  to{
    opacity: 1;
  }
`

export const spin = keyframes`
  100% {
    transform: rotate(360deg)
  }
`

export const fadeInTop = keyframes`
  from{
    opacity: 0;
    top: -10px;
  }
  to{
    opacity: 1;
    top: 0px;
  }
`
