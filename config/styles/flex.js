import styled from 'styled-components'

export const FlexBetween = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    text-align: center;
`

export const FlexCenter = styled.div`
    width:100%;
    display:flex;
    justify-content: center;
    align-items:center;
`