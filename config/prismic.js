import Prismic from 'prismic-javascript'

export const API_URL = process.env.API_URL_PRISMIC
export const API_TOKEN = process.env.NEXT_PUBLIC_PRISMIC_ACCESS_TOKEN

export  const client = Prismic.client(API_URL, { accessToken : API_TOKEN})