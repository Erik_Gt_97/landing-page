import styled from 'styled-components'
import { scrollStyled } from '../../config/styles/scroll'

export const TabsContainer = styled.div`
  max-height: 265px;
  overflow: auto;
  padding: 0px 20px;
  ${scrollStyled};
  &.full_line{
    box-shadow: 0 3px 0 -1px #BCBCFF;
  }
@media (min-width: 320px) and (max-width: 479px){
  max-height: 150px;
}
@media (min-width:480px) and (max-width: 768px){
  max-height: 120px;
}
`

export const TabContainer = styled.div`
  padding: 10px 15px;
  background: red;
  min-width: 150px;
  font-size: 14px;
  border-radius: 10px;
  margin-bottom:10px;
  align-items: center;
  cursor: pointer;
  transition: background .4s ease;
  font-weight: bold;
  color: rgba(66, 66, 66, 0.5);
  position: relative;
  user-select: none;
  background: var(--white-smoke);
  color: var(--ship-grey);
  &.active {
    background: var(--secondary);
    font-weight: normal;
    color: var(--white);
  }
  &.disabled {
    pointer-events:none;
    opacity:0.5;
  }
  :last-child{
    margin-bottom:0;
  }
  b {
    margin-left: 25px;
    font-weight: 500;
  }
  i {
    margin-left: 3px;
  }
  p {
    margin: 0;
    margin-top: 5px;
    margin-left: 12px;
    font-weight: 400;
  }
`