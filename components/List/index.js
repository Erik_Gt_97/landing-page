import React from 'react'
import PropTypes from 'prop-types'
import { TabsContainer, TabContainer } from './List.style'
import classnames from 'classnames'
const { Provider, Consumer } = React.createContext({
  value: null,
  onTab: (value) => {}
})


export function ListItem ({ children, value, disabled, click }) {
  return (
    <Consumer>
      {
        props => (
          <TabContainer
            className={classnames({ active: props.value === value, disabled })}
            onClick={() => { 
              props.onTab(value); 
              click()}}
          >
            {children}
          </TabContainer>
        )
      }
    </Consumer>
  )
}

ListItem.propTypes = {
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
}


function List ({
  children,
  value,
  onTab = () => {},
  fullLine = false,
  className = '',
  style = {}
}) {
  const classNames = classnames({
    full_line: fullLine
  }, className)

  return (
    <TabsContainer className={classNames} style={style}>
      <Provider value={{
        value,
        onTab
      }}
      >
        {children}
      </Provider>
    </TabsContainer>
  )
}

List.propTypes = {
  children: PropTypes.node.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onTab: PropTypes.func.isRequired,
  fullLine: PropTypes.bool
}

export default List
