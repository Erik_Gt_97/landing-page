import PropTypes from 'prop-types'
import { IconSearch, SearchFieldContainer, Input, IconCancel, IconLoading } from './SearchField.style'
import classnames from 'classnames'

/**
 * @typedef {object} PropsSearchField
 * @property {string=} className
 * @property {string=} fullWidth
 * @property {object=} style
 * @property {function(object):void=} onClear
 * @property {function(object):void=} onChange
 * @property {string} value
 * @property {boolean=} loading
 * @property {'field'|'search'=} mode
 * @property {string=} placeholder
 */

/**
 * @param {PropsSearchField} param0
 */
function SearchField ({ className, fullWidth, style, onClear, onChange, value, placeholder, mode, loading }) {
  const classProps = {
    fullWidth,
    field: mode === 'field',
    search: mode === 'search'
  }

  return (
    <SearchFieldContainer
      className={classnames(classProps, className)}
      style={style}
    >
      <Input
        type='text'
        onChange={onChange}
        value={value}
        className={classnames(classProps)}
        {...(placeholder && { placeholder })}
      />
      <IconSearch />
      {value && !loading && <IconCancel onClick={onClear} className={classnames(classProps, 'fa fa-times-circle')} />}
      {loading && <IconLoading className={classnames(classProps, 'fa fa-spinner')} />}
    </SearchFieldContainer>
  )
}

SearchField.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  fullWidth: PropTypes.bool,
  style: PropTypes.object,
  onClear: PropTypes.func,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  mode: PropTypes.oneOf(['field', 'search'])
}

SearchField.defaultProps = {
  fullWidth: false,
  onChange: () => {},
  onClear: () => {},
  style: {},
  className: '',
  value: '',
  loading: false,
  mode: 'search'
}

export default SearchField