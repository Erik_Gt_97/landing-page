import styled from 'styled-components'
import { spin } from '../../config/styles/keyFrames'

export const SearchFieldContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 450px;
  &.fullwidth{
    max-width: 100%;
  }
  &.search{
    border-radius: 25px;
    color: var(--dark);
    background: var(--white-smoke);
  }
  &.field{
    border-radius: 7px;
    color: var(--dark);
    height: 30px;
    background-color: var(--background-field);
    border: 1px solid var(--primary);
    &:focus{    
      outline: none;
      box-shadow: 0 0 3px 0 var(--primary);
    }
  }
`

export const Input = styled.input`
  border: 1px solid var(--border-component);
  font-size: 14px;
  outline: none;
  padding: 0 30px 0 40px;
  background-color: transparent;
  width: 100%;
  font-weight: 400;
  color: var(--ship-grey);  
  &.search{
    height: 35px;
    ::placeholder{
      color: inherit;
      opacity: .5;
    }
  }
  &.field{
    height: 30px;
    ::placeholder {
      color: var(--primary);
      opacity: .7;
    }
  }
`

export const IconSearch = styled.div.attrs(() => ({
  className: 'fa fa-search'
}))`
  height: 100%;
  width: 35px;
  position: absolute;
  left: 5px;
  font-size: 17px;
  top: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  color: var(--placeholder);
`

export const IconCancel = styled.div`
  position: absolute;
  right: 10px;
  font-size: 18px;
  z-index: 1;
  cursor: pointer;
  color: var(--placeholder); 
  &.search{
    top: 9px;
  }
  &.field{
    top: 6px;
  }
`

export const IconLoading = styled.div`
  position: absolute;
  right: 10px;
  font-size: 16px;  
  z-index: 1;
  color: var(--color-component);
  color: var(--primary);
  animation: ${spin} 1s linear infinite;
  &.search{
    top: 10px;
  }
  &.field{
    top: 7px;
  }
`