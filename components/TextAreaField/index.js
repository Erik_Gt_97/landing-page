import classnames from 'classnames'
import PropTypes from 'prop-types'
import { TextAreaContainer, Footer, TextAreaInput, HelperText } from './TextAreaField.style'

/**
 * @typedef {object} PropsTextAreaField
 * @property {string=} value
 * @property {boolean|string=} errorHelper|
 * @property {boolean=} fullWidth
 * @property {boolean=} required
 * @property {boolean=} disabled
 * @property {number=} maxLength
 * @property {string=} placeholder
  * @property {string=} helper
 * @property {'danger'|'warning'|'success'=} typeHelper
 * @property {object=} style
 * @property {string=} className
 * @property {function(any):void=} onChange
 * @param {PropsTextAreaField} param0
 */
function TextAreaField ({ value, style, className, helper, fullWidth, disabled, typeHelper, required, ...props }) {
  const typesHelpers = {
    danger: typeHelper === 'danger',
    warning: typeHelper === 'warning',
    success: typeHelper === 'success',
    helper: !!helper
  }
  const nextProps = {}
  if (value) {
    nextProps.value = value
  }
  return (
    <TextAreaContainer
      className={classnames({
        fullWidth
      }, className)}
      {...(style && { style })}
    >
      <TextAreaInput {...props} {...nextProps} className={classnames({ required, ...typesHelpers })}  />
      {!disabled &&
        <Footer>
          <HelperText
            className={classnames({
              ...typesHelpers
            })}
          >
            {helper}
          </HelperText>
          {/* <span>{value?.length}/{props.maxLength}</span> */}
        </Footer>}
    </TextAreaContainer>)
}

TextAreaField.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  fullWidth: PropTypes.bool,
  className: PropTypes.string,
  helper: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  typeHelper: PropTypes.oneOf(['success', 'warning', 'danger'])
}

TextAreaField.defaultProps = {
  maxLength: 1000,
  helper: '',
  fullWidth: false,
  required: false,
  typeHelper: 'danger',
  disabled: false
}

export default TextAreaField
