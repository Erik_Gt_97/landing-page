import styled from 'styled-components'
import { scrollStyled } from '../../config/styles/scroll'

export const TextAreaContainer = styled.div`
  position: relative;
  display: inline-block;
  input:-webkit-autofill,
  input:-webkit-autofill:hover, 
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active  {
    box-shadow: 0 0 0 30px white inset !important;
  }
  &.fullWidth {
    width: 100%;
  }
  textarea, span{
    width: 100%;
  }
  span{
    text-align: right;
    display: block;
    font-size: 12px;
    margin-top: .5em;
    color: var(--primary);
  }
`
export const TextAreaInput = styled.textarea`
  height: 200px;
  resize: none;
  border: 1px solid var(--border-field);
  font-size: 14px;
  outline: none;
  transition: .3s ease;
  background-color: var(--background-field);
  padding: 1em;
  border-radius: 5px;
  color: var(--primary);
  font-weight: 400;
  ::placeholder {
    color: var(--placeholder);
    /* opacity: .7; */
  }
  &:focus{    
    outline: none;
    box-shadow: 0 0 3px 0 var(--primary);
  }
  :read-only{
    background-color: var(--background-alt);
  }
  :disabled{
    ::placeholder {
      opacity: .4;
    } 
    resize: none;
    background-color: var(--background-alt);
  }
  width: 100%;
  &.helper {
    &.danger{
      border-color: var(--danger);
    }
    &.warning{
      border-color: var(--warning);
    }
    &.success{
      border-color: var(--success);
    }
    border: 1px solid;
  }  
  &.required {
    border-left: 3px solid var(--danger);
  }
  ${scrollStyled};
`

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  position:relative;
`

export const HelperText = styled.div`
  visibility: hidden;
  position: absolute;
  font-size:11px;
  opacity: 0;
  left: 4px;
  top: 1px;
  transition: .3s ease;
  &.danger{
    color: var(--danger);
  }
  &.warning{
    color: var(--warning);
  }
  &.success{
    color: var(--success);
  }
  &.helper{
    visibility: visible;
    bottom: -15px;
    opacity: 1;
  }
`