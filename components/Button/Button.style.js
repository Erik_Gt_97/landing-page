// @ts-nocheck
import styled, { css } from 'styled-components'
import { spin } from '../../config/styles/keyFrames'

const disabled = css`
  pointer-events: none;
  opacity: .7;
`

export const ButtonContainer = styled.button`  
  ${props => props.color === 'default' && css`
    background-color: var(--white);
  `}
  ${props => props.color !== 'default' && css`
    background-color: var(--${props.color});
  `}
  color: ${props => props.color === 'white' ? 'var(--primary)' : 'var(--white)'};
  ${props => props.color === 'default' && css`
    color: var(--black);
  `};
  border: 1px solid transparent;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
  ${props => props.minimize && css`
    height: 30px;
  `}
  ${props => !props.minimize && css`
    height: 35px;
  `}
  font-size: 16px;
  font-weight: bold;
  padding: 0 2em;
  border-radius: 33px;
  ${props => props.fullWidth && css`
    width: 100%;
  `}
  cursor: pointer;
  transition: .2s ease;
  display: flex;
  ${props => props.disabled && disabled}
  :focus{
    outline: none;
  }
  ${props => props.loading === 1 && css`
    justify-content: center;
  `}
`

export const Spinner = styled.div.attrs(() => ({
  className: 'fa fa-spinner'
}))`
  font-size: 15px;
  color: inherit;
  animation: ${spin} 1s linear infinite;
`