
import PropTypes from 'prop-types'
import { ButtonContainer, Spinner } from './Button.style'

export default function Button ({ children, loading, icon, iconPosition, invert, ...props }) {
  return (
    <ButtonContainer
      /** @ts-ignore */
      loading={loading ? 1 : 0}
      /** @ts-ignore */
      type='button'
      /** @ts-ignore */
      invert={invert}
      {...props}
    >
      {!loading && children}
      {loading && <Spinner />}
    </ButtonContainer>
  )
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  invert: PropTypes.bool,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  fullWidth: PropTypes.bool,
  color: PropTypes.oneOf(['primary', 'secondary', 'success', 'danger', 'warning', 'white', 'default']),
  minimize: PropTypes.bool
}

Button.defaultProps = {
  invert: false,
  disabled: false,
  loading: false,
  fullWidth: false,
  minimize:false,
  color: 'default'
}
