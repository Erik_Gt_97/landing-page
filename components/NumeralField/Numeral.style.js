import styled, { css } from 'styled-components'

export const NumeralFieldContainer = styled.div`
  position: relative;
  display: inline-flex;
  background: var(--background-field);
  height: 45px;
  input:-webkit-autofill,
  input:-webkit-autofill:hover, 
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active  {
    box-shadow: 0 0 0 30px white inset !important;
  }
  border-radius: 5px;
  border: 1px solid var(--border-field);
  ${props => props.helper && css`
    border: 1px solid var(--danger);
  `}
  ${props => props.fullWidth && css`
    width: 100%;
  `}
`

export const NumeralStyled = styled.input`
  width: 100%;
  border-radius: 5px;
  border: 0px;
  font-size: 13px;
  font-weight: normal;
  outline: none;
  transition: .3s ease;
  padding: 0 12px;
  background-color: transparent;
  color: var(--primary);
  font-family:'Helvetica';
  ::placeholder {
    color: var(--placeholder);
  }
  &:focus{    
    outline: none;
    box-shadow: 0 0 3px 0 var(--primary);
  }
  ${props => props.helper && css`
  &:focus{    
    outline: none;
    box-shadow: 0 0 3px 0 var(--danger);
  }
  `}
  :read-only{
    background-color: var(--background-alt);
  }
  :disabled{
    ::placeholder {
      opacity: .4;
    } 
    opacity: .6;
  } 
`
