import React from 'react'
import PropTypes from 'prop-types'
import { NumeralFieldContainer, NumeralStyled } from './Numeral.style'
import { HelperText } from '../InputField/InputField.style'
import classnames from 'classnames'

/**
 * @typedef {object} PropsNumeralField
 * @property {string=} className
 * @property {boolean=} disabled
 * @property {boolean=} minimize
 * @property {boolean=} fullWidth
 * @property {boolean=} required
 * @property {string=} placeholder
 * @property {boolean=} readOnly
 * @property {function(object):void=} onClick
 * @property {function(object):void=} onChange
 * @property {string=} helper
 * @property {'tooltip'| 'subtext'=} modeHelper
 * @property {object=} style
 * @property {boolean=} autoFocus
 * @property {string=} id
 * @property {string=} name
 * @property {number|string=} value
 * @property {number=} decimal
 * @property {number=} max
 * @property {number=} min
 * @property {boolean=} negative
 * @property {any=} inputRef
 * @property {'danger'|'warning'|'success'=} typeHelper
 * @property {function(object):void=} onKeyDown
 */

/**
 *
 * @param {PropsNumeralField} param0
 */
export default function NumeralField ({
  onChange,
  className,
  minimize,
  decimal,
  style,
  max,
  min,
  negative,
  inputRef,
  helper,
  modeHelper,
  required,
  fullWidth,
  typeHelper,
  ...props
}) {
  const handleChange = ev => {
    const val = ev.target.value
    let valParse
    if (decimal !== 0) {
      valParse = val
    } else {
      valParse = parseInt(val)
    }
    if (isNaN(valParse)) {
      ev.target.value = ''
      if (negative && val === '-') {
        ev.target.value = '-'
      }
    } else {
      if (decimal !== 0) {
        if (val.split('.')[1] !== undefined) {
          const tam = val.split('.')[1].length
          const valt = tam >= decimal
          const dec = tam !== 0
          if (dec && valt) {
            valParse = Number.parseFloat(val).toFixed(decimal)
            if (max !== undefined) {
              valParse = ev.target.value > max ? max : valParse
            }
          }
        } else {
          if (max !== undefined) {
            valParse = ev.target.value > max ? max : ev.target.value
          }
        }
      }
      ev.target.value = valParse
    }
    if (max !== undefined && ev.target.value > max) {
      ev.target.value = max
    }
    if (min !== undefined && ev.target.value < min) {
      ev.target.value = min
    }
    onChange(ev)
  }

  var nextProps = {}
  if (inputRef) {
    nextProps.ref = inputRef
  }
  /** @ts-ignore */
  if (props.value === '') {
    nextProps.value = ''
  } else {
    if (isNaN(props.value)) {
      nextProps.value = ''
    } else {
      nextProps.value = props.value
    }
  }

  const typesHelpers = {
    danger: typeHelper === 'danger',
    warning: typeHelper === 'warning',
    success: typeHelper === 'success',
    helper: !!helper
  }

  return (
    /** @ts-ignore */
    <NumeralFieldContainer className={className} helper={helper} style={style} isRequired={required} minimize={minimize} fullWidth={fullWidth}>
      <NumeralStyled
        minimize={minimize}
        {...nextProps}
        {...props}
        onChange={handleChange}
        /** @ts-ignore */
        helper={helper}
        {...(props.hasOwnProperty('value') && { value: props.value !== null ? props.value : '' })}
        autoComplete='off'
      />
      {
        modeHelper === 'subtext' && (
          <HelperText className={classnames({ ...typesHelpers })}>
            {helper}
          </HelperText>
        )
      }
    </NumeralFieldContainer>
  )
}

NumeralField.propTypes = {
  required: PropTypes.bool,
  minimize: PropTypes.bool,
  decimal: PropTypes.number,
  max: PropTypes.number,
  min: PropTypes.number,
  negative: PropTypes.bool,
  inputRef: PropTypes.any,
  onChange: PropTypes.func,
  name: PropTypes.string,
  helper: PropTypes.string,
  modeHelper: PropTypes.oneOf(['tooltip', 'subtext']),
  typeHelper: PropTypes.oneOf(['success', 'warning', 'danger'])
}

NumeralField.defaultProps = {
  required: false,
  minimize: false,
  helper: '',
  decimal: 0,
  onChange: () => {},
  modeHelper: 'subtext',
  typeHelper: 'danger'
}