import React from 'react'
import PropTypes from 'prop-types'
import { Marker } from '@react-google-maps/api'

/**
 * @typedef {object} PropsMarkerPoint
 * @property {function(object):void=} onChange
 * @property {boolean=} draggable
 * @property {object} position
 * @property {function(object):void=} onDragEnd
 */

/**
 * @param {PropsMarkerPoint} param0
 */
function MarkerPoint ({ onChange, position, draggable, ...props }) {
  function getPosition () {
    return position
  }
  const onLoad = marker => {
  //  console.log('marker: ', marker)
  }
  return (
    <Marker
      onLoad={onLoad}
      position={getPosition()}
      {...props}
      draggable={draggable}
    />
  )
}

MarkerPoint.propTypes = {
  position: PropTypes.object.isRequired,
  draggable: PropTypes.bool
}

MarkerPoint.defaultProps = {
  draggable: false
}

export default MarkerPoint