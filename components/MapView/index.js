// @ts-nocheck
import React from 'react'
import PropTypes from 'prop-types'
import { MapViewContainer } from './MapView.style'
import { GoogleMap } from '@react-google-maps/api'
import MarkerPoint from './MarkerPoint'
import LoadScriptOnlyIfNeeded from './LoadScripOnlyIfNeeded'
import { geocode, getGeocodeByLatLng } from './utils'

export {
  geocode,
  getGeocodeByLatLng,
  MarkerPoint
}

const LIBRARIES = ['drawing', 'places']

/**
 * @typedef {object} PropsMapView
 * @property {string=} className
 * @property {object=} style
 * @property {node=} children
 * @property {function(object):void=} onLoad
 * @property {function(object):void=} onClick
 * @property {object} center
 * @property {string=} name
 * @property {number=} zoom
 * @property {string=} height
 */

/**
 * @param {PropsMapView & { children: any }} param0
 */

function MapView ({ className, style, children, center, zoom, onLoad, height, google_api_key, ...props }) {
  return (
    <MapViewContainer className={className} height={height} style={style}>
      <LoadScriptOnlyIfNeeded
        id='loader-map'
        version='3'
        language='en'
        region='US'
        preventGoogleFontsLoading
        googleMapsApiKey={google_api_key}
        libraries={LIBRARIES}
      >
        <GoogleMap
          id='google-map'
          center={center}
          options={{
            zoomControl: false,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: true,
            rotateControl: false,
            fullscreenControl: false,
            clickableIcons: false
          }}
          zoom={zoom}
          defaultCenter
          {...props}
          onLoad={onLoad}
        >
          {children}
        </GoogleMap>
      </LoadScriptOnlyIfNeeded>
    </MapViewContainer>
  )
}

MapView.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object,
  center: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired
  }),
  google_api_key: PropTypes.string,
  zoom: PropTypes.number,
  onSearch: PropTypes.func,
  onLoad: PropTypes.func,
  height: PropTypes.string
}

MapView.defaultProps = {
  center: {
    lat: 38.2850563,
    lng: -100.3448311
  },
  zoom: 5,
  onLoad: () => {},
  height: '300px'
}

export default MapView