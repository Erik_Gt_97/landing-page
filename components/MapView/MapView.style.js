// @ts-nocheck
import styled from 'styled-components'

export const MapViewContainer = styled.div`
  width: 100%;
  height: ${props => props.height};
  position: absolute;
  #google-map{
    height: 100%
  };
  .gmnoprint {
      display : none;
  }
`


