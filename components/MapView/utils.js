import Axios from 'axios'

const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY
export async function geocode (address) {
 console.log(GOOGLE_API_KEY)
  const geocoder = new window.google.maps.Geocoder()
  const promise = new Promise((resolve, reject) => {
    geocoder.geocode({ address: address }, function (results, status) {
      if (status === window.google.maps.GeocoderStatus.OK) {
        resolve(results)
      } else {
        reject(new Error('Address not found'))
      }
    })
  })
  return promise
}
export const getGeocodeByLatLng = (lat, lng) => {
  const latLngString = `latlng=${lat},${lng}`
  return Axios.get(`https://maps.googleapis.com/maps/api/geocode/json?${latLngString}&key=${GOOGLE_API_KEY}`)
}