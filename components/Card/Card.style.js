import styled from 'styled-components'

export const Wrapper = styled.div`
    display: grid;
    align-items: center;
    grid-template-columns: repeat(1,1fr);
    row-gap: 30px;
    img {
        height: 80px;
        width: 80px;
    }
    p {
        color: var(--white);
        font-weight: 500;
        font-size: 14px;
    }
    div {
        display: flex;
        align-items: center;
        justify-content: center;
    }
    @media (min-width: 768px){
        grid-template-columns: 20% 80%;
        div {
            display: block;
        }
        .wrapper_img {
            display: flex;
            align-items: center;
            justify-content: center;
        }
    }
`