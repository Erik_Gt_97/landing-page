import { Wrapper } from './Card.style'

export default function Card({children, image_url, name}) {
    return (
        <div>
        <Wrapper>
            <div className='wrapper_img' >
            <img height="80px" width="80px" src={image_url} alt={name}></img>
            </div>
            <div>
            <p>
             {children}
            </p>
            </div>
        </Wrapper>
        </div>
    )
}
