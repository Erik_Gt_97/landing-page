// @ts-nocheck
import React from 'react'
import PropTypes from 'prop-types'
import {
  SelectFieldContainer, CaptionContainer, OptionContainer, SearchContainer,
  Image, ImageHeader, Result, OptionsFluid, Input, IconDown, IconSearch, IconLoading
} from './SelectField.style'
import { HelperText } from '../InputField/InputField.style'

import classnames from 'classnames'
const { Provider, Consumer } = React.createContext(false)
/**
 *
 * @template T
 * @typedef {object} PropsOption
 * @property {string|number|boolean} value
 * @property {boolean=} readOnly
 * @property {string=} image
 */

/**
 * @param {PropsOption & { children: any }} param0
 *
 */
export function Option ({ value, children, image, readOnly }) {
  const image_res = {}
  if (image !== undefined) {
    image_res.image = image
  }
  return (
    <Consumer>
      {
        props => (
          <OptionContainer
            onClick={() => props.handleSelect({ value, label: children, ...image_res })}
            readOnly={readOnly}
          >
            {image && <Image src={image} />}
            {children}
          </OptionContainer>
        )
      }
    </Consumer>
  )
}

Option.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]).isRequired,
  image: PropTypes.string,
  readOnly: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired
}

Option.defaultProps = {
  readOnly: false
}

/**
 * @typedef {object} PropsSelectField
 * @property {object|boolean=} option
 * @property {function(object):void=} onChange
 * @property {boolean=} fullWidth
 * @property {boolean=} loading
 * @property {boolean=} loadingSearch
 * @property {boolean=} disabled
 * @property {boolean=} readOnly
 * @property {string=} className
 * @property {string=} id
 * @property {object=} style
 * @property {boolean=} light
 * @property {string=} minWidth
 * @property {function(string):void=} onSearch
 * @property {boolean=} search
 * @property {string=} placeholder
 * @property {string=} name
 * @property {string=} helper
 * @property {boolean=} required
 * @property {boolean=} underline
 * @property {'danger'|'warning'|'success'=} typeHelper
 *
 */

/**
 * @param {PropsSelectField & { children: any } } param0
 */
function SelectField ({
  children,
  option,
  onChange,
  fullWidth,
  loading,
  loadingSearch,
  disabled,
  className,
  style,
  light,
  minWidth,
  onSearch,
  search,
  name,
  required,
  readOnly,
  underline,
  typeHelper,
  ...props
}) {
  const [open, setOpen] = React.useState(false)
  const [textSearch, setText] = React.useState('')
  const [id] = React.useState(`SELECT_FIELD`)
  const handleOut = React.useCallback(ev => {
    if (readOnly || disabled) return
    if (ev.target.closest('.' + id)) {
      setOpen(true)
    } else {
      setOpen(false)
    }
  }, [readOnly || disabled])

  React.useEffect(() => {
    document.addEventListener('click', handleOut)
    return () => {
      document.removeEventListener('click', handleOut)
    }
  }, [handleOut])
  function handleChange (ev) {
    onSearch(ev.target.value)
    setText(ev.target.value)
  }
  function handleSelect (_option) {
    onChange(_option)
    setOpen(false)
  }

  function handleBlurSearch () {
    setOpen(false)
    setText('')
    onSearch('')
  }

  const typesHelpers = {
    danger: typeHelper === 'danger',
    warning: typeHelper === 'warning',
    success: typeHelper === 'success',
    helper: !!props.helper
  }

  return (
    <SelectFieldContainer
      className={` ${className}`}
      light={light}
      fullWidth={fullWidth}
      style={style}
    >
      {option && option.image && <ImageHeader src={option.image} />}
      <Input
        {...props}
        autoComplete='off'
        readOnly
        className={id}
        underline={underline}
        value={option ? option.label : ''}
        img={option ? option.image : false}
        onFocus={handleOut}
        fullWidth={fullWidth}
        required={required}
        disabled={disabled}
        forReadOnly={readOnly}
        minWidth={minWidth}
        onChange={() => { }}
      />
      {loading && <IconLoading className='fa fa-spinner' />}
      <input type='hidden' value={option ? option.value : ''} name={name} onChange={() => { }} />
      <Provider value={{ option, handleSelect }}>
        <Result className={classnames({ show: open })}>
          {
            search &&
              <SearchContainer>
                <Input
                  {...props}
                  className={id}
                  search
                  placeholder='Buscar...'
                  value={search ? textSearch : ''}
                  onChange={handleChange}
                  onBlur={handleBlurSearch}
                  autoComplete='off'
                  img={option ? option.image : false}
                  fullWidth
                />
                {loadingSearch && <IconLoading className='fa fa-spinner' />}
                {!loadingSearch && <IconSearch />}
              </SearchContainer>
          }
          <OptionsFluid search={search}>
            {children}
          </OptionsFluid>
        </Result>
      </Provider>
      {(!loading) && <IconDown disabled={readOnly || disabled} />}
      <HelperText className={classnames({ ...typesHelpers })}>{props.helper}</HelperText>
    </SelectFieldContainer>
  )
}

SelectField.propTypes = {
  option: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  fullWidth: PropTypes.bool,
  name: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  loading: PropTypes.bool,
  loadingSearch: PropTypes.bool,
  children: PropTypes.node,
  minWidth: PropTypes.string,
  helper: PropTypes.string,
  light: PropTypes.bool,
  disabled: PropTypes.bool,
  search: PropTypes.bool,
  onSearch: PropTypes.func,
  required: PropTypes.bool,
  id: PropTypes.string,
  readOnly: PropTypes.bool,
  underline: PropTypes.bool,
  typeHelper: PropTypes.oneOf(['success', 'warning', 'danger'])
}

SelectField.defaultProps = {
  option: null,
  fullWidth: false,
  className: '',
  onChange: () => { },
  search: false,
  onSearch: () => { },
  minWidth: '',
  loading: false,
  loadingSearch: false,
  disabled: false,
  helper: '',
  light: false,
  readOnly: false,
  underline: false,
  typeHelper: 'danger'
}

export default SelectField

/**
 * @typedef {object} PropsCaption
 * @property {any} children
 *
 * @param {PropsCaption} param0
 */

export function Caption ({ children }) {
  return (
    <CaptionContainer>
      {children}
    </CaptionContainer>
  )
}

Caption.propTypes = {
  children: PropTypes.node.isRequired
}
