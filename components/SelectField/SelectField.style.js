// @ts-nocheck
import styled, { css } from 'styled-components'
import { spin } from '../../config/styles/keyFrames'
import { scrollStyled } from '../../config/styles/scroll'

export const SelectFieldContainer = styled.div`
  ${props => props.fullWidth && css`
    width: 100%;
  `}
  ${props => props.light && css`
    --color-component: var(--white);
    --color-icon: var(--white);
    --color-placeholder: var(--white);
  `}

  ${props => !props.light && css`
    --color-component: var(--secondary);
    --color-placeholder: var(--text-black-50);
    --color-icon:  var(--text-black-50);
    background:var(--white);
  `}
  position: relative;
  display: inline-block;
  box-sizing: border-box;
`
export const Input = styled.input`
  user-select:none;
  position:relative;
  ${props => props.minWidth && css`
    min-width: ${props => props.minWidth};
  `}
  ${props => props.fullWidth && css`
    width: 100%;
  `}  
  height: 45px;
  cursor: pointer;
  ${props => props.search && css`
    --color-component: var(--secondary);
    --color-placeholder: var(--text-black-50);
    --color-icon:  var(--text-black-50);
    height:30px;
    border-radius:5px;
    cursor: text;
  `}
  border: 1px solid var(--border-field);
  border-radius: 5px;
  ${props => props.underline && css`
    border-radius: 0px;
    border: 0px;
    border-bottom: 1px solid var(--primary);
  `}  
  font-size: 14px;
  outline: none;
  transition: .2s ease-out;
  transition: 0ms padding;
  padding: 0 12px;
  ${props => props.img && css`
    padding-left: 30px;
  `}  
  position: relative;
  background-color: transparent;
  z-index: 2;
  color:var(--primary);
  background-color: var(--background-field);
  ::placeholder {
    color:var(--placeholder);
    /* opacity: .7; */
  }
  :focus{
    ${props => !props.underline && css`
     box-shadow: 0 0 3px 0 var(--primary);
    `}
    ${props => props.helper && css`
    box-shadow: 0 0 3px 0 var(--danger);
    `}
  }
  :disabled{
    cursor:auto;
    opacity: .6;
  }
  
  ${props => props.forReadOnly && css`
    background-color: var(--background-alt);
    cursor:auto;
  `}
  ${props => props.helper && css`
    border: 1px solid var(--danger);
  `}
  ${props => props.required && css`
    border-left: 3px solid var(--danger);
  `}
`
export const IconDown = styled.div.attrs(() => ({
  className: 'fa fa-angle-down'
}))`
  position: absolute;
  right: .8em;
  top: 12px;
  z-index: 2;
  color: var(--placeholder);
  ${props => props.disabled && css`
    display:none;
  `}
`

export const Result = styled.div.attrs(() => ({
  className: 'result'
}))`
  position: absolute;
  max-height: 200px;
  background-color: var(--white);
  width: 100%;
  top: 45px;
  transition: .1s ease-out;
  box-shadow: var(--shadow);
  overflow: hidden;
  z-index: 5;
  color: var(--text-dark);
  opacity: 0;
  visibility: hidden;
  &.show{
    opacity: 1; 
    top: 45px;
    visibility: visible;
  }
  z-index:8;
`

export const OptionsFluid = styled.div`
  ${scrollStyled}
  max-height: 200px;
  ${props => props.search && css`
    max-height: 158px;
  `}
  color: var(--text-dark);
  overflow: auto;
`
export const OptionContainer = styled.ul`
  padding: .8em 1.5em;
  font-size: 14px;
  transition: .3s ease;
  position: relative;
  display: flex;
  align-items: center;
  margin: 0;
  font-weight: 400;
  ${props => props.multiple && css`
    padding: 1em 1.5em;
  `}
  cursor: pointer;
  :hover{
    background-color: var(--background-alt);
  }
  ${props => props.readOnly && css`
    pointer-events: none;
  `}
`
export const CaptionContainer = styled.ul`
  padding: .8em 1.5em;
  font-size: 13px;
  transition: .3s ease;
  position: relative;
  display: flex;
  align-items: center;
  ${props => props.multiple && css`
    padding: 1em 1.5em;
  `}
  background-color: var(--background-alt);
`

export const SearchContainer = styled.li`
  padding: .5em;
  font-size: 13px;
  transition: .3s ease;
  position: relative;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  ${props => props.multiple && css`
    padding: 1em 1.5em;
  `}
  cursor: pointer;
  :hover{
    background-color: var(--background-alt);
  }
`
export const IconSearch = styled.div.attrs(() => ({
  className: 'fa fa-search'
}))`
  position: absolute;
  right: 1em;
  top: 14px;
  z-index: 3;
  color: var(--text-black-50);
`
export const Image = styled.img`
  width: 17px;
  height: 17px;
  margin-right: 5px;
  border-radius:50%;
  object-fit: cover;
`
export const ImageHeader = styled.img`
  width: 17px;
  height: 17px;
  margin-right: 5px;
  border-radius:5px;
  top:9px;
  left:9px;
  object-fit: cover;
  position:absolute;
  z-index: 3;
`
export const LoadingContainer = styled.div`
  position: absolute;
  right: 11px;
  top: 14px;
  z-index: 1;
  color: var(--color-component);
`

export const IconLoading = styled.div`
  position: absolute;
  right: 10px;
  font-size: 16px;  
  z-index: 2;
  color: var(--primary); 
  animation: ${spin} 1s linear infinite;
  display: flex;
  top: 0;
  height:100%;
  justify-content: center;
  align-items: center;
`