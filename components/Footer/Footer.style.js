import styled from 'styled-components'
import { scrollStyled } from '../../config/styles/scroll'

export const Container = styled.div`
    padding: .5em 2em 2em 2em;
    overflow : auto;
    max-height: 60vh;
    ${scrollStyled};
    color: var(--charcoal);
    p{
        font-weight: 400;
        font-size: 14px;
    }
`