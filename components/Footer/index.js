import { useState } from 'react'
import Modal, {ModalBody, ModalHeader} from '../Modal'
import styles from './Footer.module.css'
import {Container} from './Footer.style'
import ReactHtmlParser from 'react-html-parser'
import { cleanText } from '../../config/tools/utils'

export default function Footer( {
  personal_data_policy,
  terms_conditions, 
  policy_terms
}) {
    const [open, setOpen] = useState(false)
    const [title, setTitle] = useState('TÉRMINOS Y CONDICIONES')
    const [option, setOption] = useState(1)

    function handleClick(opt){
      setOption(opt)
      switch (opt) {
        case 1:
          setTitle('TÉRMINOS Y CONDICIONES')
          break
        case 2:
          setTitle('POLITICA DE DATOS PERSONALES')
          break
      }
      setOpen(true)
    }

    function getData(op) {
      switch (op){
        case 1 :
          return terms_conditions
        case 2 :
          return personal_data_policy
      }
    }
    return (
      <>
      <Modal size='lg' open={open} >
        <ModalHeader title={title} onClose={()=> setOpen(false)} />
        <ModalBody px={0} py={0}>
          <Container>
           {
             getData(option).map((data) => 
              ReactHtmlParser(cleanText(data.text))
             )
           }
          </Container>
        </ModalBody>
      </Modal>
        <footer  className={styles.footer}>
          <div style={{display:'flex',alignItems:'center', justifyContent:'center',background:'var(--primary)'}}>
        <div className={styles['footer__top']} >
          <div  className={styles['footer__top__logo']}>
            <div className={styles['wrapper_logo']}>
              <img height='40px' width="150px" src="/Logo-footer.png" alt="Logo"></img>
            </div>
            <div>
             <div  className={styles['flex_center_information']}>
               <div style={{fontSize:'14px'}}  >
               <b  style={{margin:0}} >Politica y Términos</b>
               <p style={{ cursor:'pointer', color: '#CECECE'}} onClick={()=> handleClick(1)}>Términos y condiciones</p>
               <p style={{margin:0,cursor:'pointer', color: '#CECECE'}} onClick={()=> handleClick(2)}>Politica de Datos Personales</p>
               </div>
             </div>
            </div>
          </div>
          <div className={styles['footer__top__information']}>
            <a className={styles['wrapper_information']}>
              <img height="40px" width="38px" alt="Trabaja con nosotros" src="/Work.png"></img>
              <p className='ml-2'>Trabaja con nosotros</p>
            </a>
            <a className={styles['wrapper_information']}>
              <img height="40px" width="38px" alt="Libro de reclamaciones" src="/Book.png"></img>
              <p className='ml-2'>Libro de reclamaciones</p>
            </a>
          </div>
          </div>
          </div>
        <div className={styles['footer__bottom']}> 
        Copyright 2021 - Inversiones La Cruz. Todos los derechos reservados.
        </div>
      </footer>
      </>
    )
}
