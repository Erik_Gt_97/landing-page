// @ts-nocheck
import styled, { css } from 'styled-components'
import { fadeIn } from '../../config/styles/keyFrames'

export const ModalContainer = styled.form`
  background-color: var(--white);
  outline: none;
  position: relative;
  transition: .3s ease;
  border-radius: 15px;
  margin: 2em;
  ${props => props.size === 'fluid' && css`
    width: 100%;
  `}
  ${props => props.size === 'xl' && css`
    width: 100%;
    max-width: 1140px;
  `}
  ${props => props.size === 'lg' && css`
    width: 100%;
    max-width: 960px;
  `}
  ${props => props.size === 'md' && css`
    width: 100%;
    max-width: 720px;
  `}
  ${props => props.size === 'sm' && css`
    width: 100%;
    max-width: 540px;
  `}
`

export const WrapperIconClose = styled.div`
  border-radius: 50%;
  /* background : #9B9B9B; */
  width: 36px;
  height: 36px;
  position: absolute;
  top: 2.1em;
  right: 2em;
  cursor: pointer;
  div {
    display: flex;
    justify-content:center;
    align-items:center;
    text-align: center;
    width: 100%;
    height: 100%;
  }

  @media (min-width:320px) and (max-width: 480px){
    top: .5em;
    right: 1em;
  }
`

export const ModalIconClose = styled.i.attrs(() => ({
  className: 'fa fa-times'
}))`
  font-size: 20px;
  cursor: pointer;
  color: var(--white);
`

export const ModalHeaderContainer = styled.div`
  border-bottom: 1px solid var(--border);
  position: relative;
  padding: 1em 2em;
  width: 100%;
  ${props => props.center && css`
    text-align: center;
  `}
  h1, h2{
    font-family: 'DIN Next LT Pro';
  }  
  h1{
    font-size: 30px;
    font-weight: 700;
    color: var(--secondary);
    margin-bottom: 0;
  }
  h2{
    margin-top: 5px;
    font-size: 12px;
    font-weight: normal;
  }
  ${props => props.large && css`
    padding: 1em 3em;
    h1{
      font-size: 26px;
    } 
    h2{
      font-size: 16px;
    }
    i{
      top: 22px;
    }
  `}
`

export const ModalBody = styled.div.attrs(props => ({
  py: props.py || props.py === 0 ? props.py : 1,
  px: props.px || props.px === 0 ? props.px : 1,
  clean: !!props.clean
}))`
  ${props => !props.clean && css`
    padding: ${props.py}em ${props.px}em;
  `}
  width: 100%;
  transition: .3s ease;
  animation: ${fadeIn} 1s ease;
`

export const ModalFooter = styled.div`
  padding: 1em;
  width: 100%;
  display: flex;
  justify-content: center;
  animation: ${fadeIn} 1s ease;
  border-top: 1px solid var(--border);
  button{
    min-width: 150px;
  }
`