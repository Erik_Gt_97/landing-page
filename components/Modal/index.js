// @ts-nocheck
import React from 'react'
import PropTypes from 'prop-types'
import { Modal as ModalUI } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Fade from '@material-ui/core/Fade'
import { ModalContainer, ModalIconClose, ModalHeaderContainer, ModalBody, ModalFooter, WrapperIconClose } from './Modal.style'

export function ModalHeader ({ title, subTitle, onClose, center, large }) {
  return (
    <ModalHeaderContainer center={center} large={large}>
      <h1>{title}</h1>
      {subTitle && <h2>{subTitle}</h2>}
      <WrapperIconClose onClick={()=> onClose()}>
        <div>
          <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M30.7336 30.7332C23.7103 37.755 12.2885 37.755 5.2652 30.7332C-1.75507 23.7113 -1.75507 12.2865 5.2652 5.26472C8.77685 1.75455 13.3885 0.000274658 18.0002 0.000274658C22.6119 0.000274658 27.222 1.75462 30.7336 5.26472C37.7555 12.2865 37.7555 23.7113 30.7336 30.7332ZM25.4271 12.6932C26.0139 12.1064 26.0139 11.158 25.4271 10.5712C25.1345 10.2786 24.7503 10.1315 24.3661 10.1315C23.982 10.1315 23.5977 10.2786 23.3051 10.5712L18.0001 15.8777L12.6966 10.5728C12.4025 10.2801 12.0183 10.133 11.6356 10.133C11.2515 10.133 10.8672 10.2801 10.5746 10.5728C9.98785 11.1595 9.98785 12.1095 10.5746 12.6947L15.8781 17.9997L10.5731 23.3047C9.98637 23.8914 9.98637 24.8414 10.5731 25.4266C11.1584 26.0134 12.1083 26.0134 12.6951 25.4266L18.0001 20.1217L23.305 25.4266C23.8918 26.0134 24.8402 26.0134 25.427 25.4266C26.0137 24.8414 26.0137 23.8914 25.427 23.3047L20.122 17.9997L25.4271 12.6932Z" fill="#9B9B9B"/>
</svg>
          </div>
      
      </WrapperIconClose>
      

    </ModalHeaderContainer>
  )
}

ModalHeader.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  onClose: PropTypes.func,
  center: PropTypes.bool,
  large: PropTypes.bool
}

ModalHeader.defaultProps = {
  onClose: () => {},
  center: false,
  large: false
}

export {
  ModalIconClose,
  ModalBody,
  ModalFooter
}

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper
  }
}))

/**
 * @typedef {object} PropsModal
 * @property {boolean} open
 * @property {function():void=} onClose
 * @property {function(object):void=} onSubmit
 * @property {'xl'|'lg'|'md'|'sm'|'auto'|'fluid'=} size
 */

/**
  * @param {PropsModal & { children: any }} param0
  */
function Modal ({ open, onClose, children, onSubmit, size }) {
  const classes = useStyles()
  function handleClose () {
    onClose()
  }

  function handleSubmit (ev) {
    ev.preventDefault()
    onSubmit(ev)
  }

  return (
    <ModalUI
      open={open}
      className={`Modal ${classes.modal}`}
      onClose={handleClose}
    >
      <Fade in={open}>
        <ModalContainer className={classes.paper} onSubmit={handleSubmit} size={size}>
          {children}
        </ModalContainer>
      </Fade>
    </ModalUI>
  )
}

Modal.propTypes = {
  open: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func,
  size: PropTypes.oneOf(['xl', 'lg', 'md', 'sm', 'auto', 'fluid'])
}

Modal.defaultProps = {
  onSubmit: () => {},
  onClose: () => {},
  size: 'auto'
}

export default Modal