import { useState } from 'react'
import { NavbarContainer, NavbarLinks, WrapperNavbar } from './Navbar.style'

export default function Navbar(){
    const [show, setShow] = useState(false)
    return(
    <WrapperNavbar>
    <NavbarContainer>
        <div className="nav-header">
          <div className="nav-title">
            <a href="#home"><img height="40px" width="150px" src='/Logo.png' alt="Logo"></img></a>
          </div>
        </div>
        <div onClick={() => setShow(!show)} className='nav-btn'>
          {!show &&  <i className='fa fa-bars'></i>}
          {show &&  <i className='fa fa-times'></i>}
        </div>
        
        <NavbarLinks show={show}>
            <a onClick={() => setShow(false)} href="#about" >¿Quiénes somos?</a>
            <a onClick={() => setShow(false)}  href="#services">Nuestros servicios</a>
            <a onClick={() => setShow(false)}  href="#ubication">Ubícanos</a>
            <a onClick={() => setShow(false)}  href="#contact">Contáctanos</a>
        </NavbarLinks>
    </NavbarContainer> 
    </WrapperNavbar>
    )
}