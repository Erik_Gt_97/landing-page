import styled, { css } from 'styled-components'

export const WrapperNavbar = styled.div`
    height: 70px;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.17);
    position: fixed;
    z-index: 1000;
    background: var(--white);
`

export const NavbarContainer = styled.nav`
    height: 70px;
    width: 100%;
    background-color: var(--white);
    z-index: 1000;
    display: flex; 
    text-align: center;
    justify-content: space-between;
    align-items: center;
    padding: 0px 3em;
    max-width: 1200px;
    > .nav-header{
        display: inline;
    }

    > .nav-header > .nav-title {
        display: inline-block;
        font-size: 22px;
        color: red;
    }

    > .nav-btn {
        display: none;
    }

    @media(min-width: 726px) and (max-width: 1024px){
        padding: 0px 2em;
    }



    @media (max-width: 725px){
        padding: 0px 2em;
        > .nav-btn {
            display: inline-block;
            cursor: pointer;
            i {
                font-size: 20px;
                color: var(--charcoal);
            }
        }
    }
`

export const NavbarLinks = styled.div`
    height: 100%;
    display: flex;
    > a {
            display:flex;
            align-items:center;
            font-weight: 500;
            font-size: 16px;
            padding: 15px;
            color: var(--ship-grey);
            :hover {
                color: var(--secondary);
                border-bottom: 3px solid var(--secondary);
                font-weight: bold;
            }
    }

    @media (max-width: 725px){
            position: absolute;
            display: block;
            width: 100%;
            background-color: white;
            height: 0px;
            transition: all 0.3s ease-in;
            overflow-y: hidden;
            top: 70px;
            left: 0px;
            padding: 0 2em;
            ${props => props.show  && css`
                height: calc(100vh - 70px);
                overflow-y: hidden;
            `}

            > a {
                padding: 15px 0em;
                font-size: 24px;
                :hover {
                    border-bottom: 3px solid var(--secondary);   
                }  
            }
    }

    @media(min-width: 726px) and (max-width: 1024px){
        max-width: 400px;
    }

    
`