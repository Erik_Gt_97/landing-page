import PropTypes from 'prop-types'
import { AlertContainer } from './Alert.style'

function Alert ({ children, ...props }) {
  return (
    <AlertContainer {...props}>
      {children}
    </AlertContainer>
  )
}

Alert.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.oneOf(['success', 'danger']),
  show: PropTypes.bool
}

Alert.defaultProps = {
  type: 'danger',
  show: true
}

export default Alert