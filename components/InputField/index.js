import React from 'react'
import PropTypes from 'prop-types'
import { InputFieldContainer, Input, HelperText } from './InputField.style'
import classnames from 'classnames'

/**
 * @typedef {object} PropsInputField
 * @property {string=} className
 * @property {string=} type
 * @property {boolean=} fullWidth
 * @property {boolean=} required
 * @property {string=} placeholder
 * @property {boolean=} readOnly
 * @property {function(object):void=} onClick
 * @property {function(object):void=} onChange
 * @property {string=} helper
 * @property {'danger'|'warning'|'success'=} typeHelper
 * @property {object=} style
 * @property {boolean=} autoFocus
 * @property {string=} id
 * @property {boolean=} disabled
 * @property {string=} name
 * @property {string|number=} value
 * @property {string=} autoComplete
 */

/**
 * @param {PropsInputField} param0
 */
function InputField ({ className, style, fullWidth, required, helper, typeHelper, ...props }) {
  const typesHelpers = {
    danger: typeHelper === 'danger',
    warning: typeHelper === 'warning',
    success: typeHelper === 'success',
    helper: !!helper
  }
  return (
    <InputFieldContainer
      className={classnames({
        fullWidth
      }, className)}
      {...(style && { style })}
    >
      <Input
        {...props}
        className={classnames({ required, ...typesHelpers })}
        autoComplete="off"
      />
      <HelperText
        className={classnames({
          ...typesHelpers
        })}
        title={helper}
      >
        {helper}
      </HelperText>
    </InputFieldContainer>
  )
}

InputField.propTypes = {
  fullWidth: PropTypes.bool,
  style: PropTypes.object,
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  helper: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  typeHelper: PropTypes.oneOf(['success', 'warning', 'danger'])
}

InputField.defaultProps = {
  helper: '',
  fullWidth: false,
  required: false,
  typeHelper: 'danger',
  disabled: false
}

export default InputField