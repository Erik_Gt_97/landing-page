import styled from 'styled-components'

export const InputFieldContainer = styled.div`
  position: relative;
  display: inline-block;
  input:-webkit-autofill,
  input:-webkit-autofill:hover, 
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active  {
    box-shadow: 0 0 0 30px white inset !important;
  }
  &.fullWidth {
    width: 100%;
  }
`

export const Input = styled.input`
  width: 100%;
  height: 45px;
  border: 1px solid var(--border-field);
  font-size: 14px;
  outline: none;
  transition: .3s ease;
  background-color: var(--background-field);
  padding: 0 12px;
  border-radius: 5px;
  color: var(--primary);
  font-weight: 400;
  ::placeholder {
    color: var(--placeholder);
    text-overflow: ellipsis;
    /* opacity: .7; */
  }
  &:focus{
    outline: none;
    box-shadow: 0 0 3px 0 var(--primary);
  }
  :read-only{
    background-color: var(--background-read-only);
    color: var(--text-read-only);
    border-color: transparent;
    &:focus{
      box-shadow: 0 0 0 1px var(--border);
    }
  }


  &.helper {
    &.danger{
      border-color: var(--danger);
      &:focus{
        outline: none;
        box-shadow: 0 0 3px 0 var(--danger);
      }
    }
    &.warning{
      border-color: var(--warning);
    }
    &.success{
      border-color: var(--success);
    }
    border: 1px solid;
  }  
  &.required {
    border-left: 3px solid var(--danger);
  }
`

export const HelperText = styled.div`
  visibility: hidden;
  position: absolute;
  font-size: 9px;
  font-family: 'Helvetica';
  opacity: 0;
  bottom: -7px;
  width: 100%;
  left: 4px;
  transition: .3s ease;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  &.danger{
    color: var(--danger);
  }
  &.warning{
    color: var(--warning);
  }
  &.success{
    color: var(--success);
  }
  &.helper{
    visibility: visible;
    bottom: -12px;
    opacity: 1;
  }
`