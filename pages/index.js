import Footer from '../components/Footer'
import Hero from '../sections/Hero'
import Contact from '../sections/Contact'
import About from '../sections/About'
import Services from '../sections/Services'
import Steps from '../sections/Steps'
import Ubication from '../sections/Ubication'
import Prismic from 'prismic-javascript'
import { client } from '../config/prismic'
import Navbar from '../components/Navbar'

export default function Home({ google_api_key,
  slogan, 
  presentation_text,
  mision,
  mision_img_url, 
  vision, 
  vision_img_url,
  about_img_url,
  about,
  offices,
  markers,
  services,
  contact_img_url,
  presentation_img_url,
  presentation_logo_img_url,
  personal_data_policy,
  terms_conditions, 
  policy_terms }) {
  return (
    <div >
      <Navbar />
      <Hero slogan={slogan} presentation_text={presentation_text} presentation_img_url={presentation_img_url} presentation_logo_img_url={presentation_logo_img_url}/>
      <About mision={mision} mision_img_url={mision_img_url} vision_img_url={vision_img_url} vision={vision} about_img_url={about_img_url} about={about} />
      <Services services={services} />
      <Steps />
      <Ubication google_api_key={google_api_key} offices={offices} markers={markers} />
      <Contact contact_img_url={contact_img_url} />
      <Footer policy_terms={policy_terms} personal_data_policy={personal_data_policy} terms_conditions={terms_conditions}/>
    </div>
  )
}

export async function getStaticProps (){
  const google_api_key = process.env.GOOGLE_API_KEY
  const { mision, vision,slogan,presentation_text, about, about_img,mision_img,vision_img,contact_img, presentation_img, presentation_logo_img, policy_terms, terms_conditions, personal_data_policy } = await (await client.getSingle('information')).data

  const res = await client.query(
    Prismic.Predicates.at("document.type","offices"),
    { pageSize: 100, orderings: "[my.offices.nombre]" }
  )
  const resServices = await client.query(
    Prismic.Predicates.at("document.type","services"),
    {pageSize: 100, orderings: "[document.first_publication_date]"}
  )

  
  const services = []
  const offices = []
  const markers = []
  res.results.map(result => {
    offices.push({
      id: result.id,
      nombre: result.data.nombre,
      direccion: result.data.direccion,
      slug: result.data.slug,
      latitud: Number(result.data.latitud),
      longitud: Number(result.data.longitud)
    })
    markers.push({
      lat: Number(result.data.latitud),
      lng: Number(result.data.longitud)
    })
  })
  resServices.results.map(result => {
    services.push({
      id: result.id,
      image_alt: result.data.image.alt | '',
      image_url: result.data.image.url,
      description: result.data.description
   })
  })

  return {
      props: {
          google_api_key,
          mision: mision[0].text,
          vision: vision[0].text,
          about: about,
          presentation_text: presentation_text,
          slogan: slogan,
          about_img_url: about_img.url,
          mision_img_url: mision_img.url,
          vision_img_url: vision_img.url,
          contact_img_url: contact_img.url,
          presentation_img_url: presentation_img.url,
          presentation_logo_img_url: presentation_logo_img.url,
          offices,
          markers,
          services,
          policy_terms,
          terms_conditions,
          personal_data_policy
      }
  }
}