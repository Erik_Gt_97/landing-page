import Document, { Html, Head, Main, NextScript } from 'next/document'
import Link from "next/link"

class MyDocument extends Document {
  render() {
    return (
      <Html lang="es">
        <Head >
            {/*favicon*/}
            {/*webfont*/}
            <link rel="preload" href="/fonts/DINNextLTPro-Medium.ttf" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/DINNextLTPro-Regular.ttf" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/DINNextLTPro-Bold.ttf" as="font" crossOrigin="" />
            {/*stylesheet*/}
            {/*script.js*/}
        </Head>
        <body className='my-body-class'>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument