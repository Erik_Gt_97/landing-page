import Head from 'next/head'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
  <>
  <Head>
        <title>Pro Custodia</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"></link>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Somos una empresa con amplio conocimiento en la tasación y custodia de garantías de joyas de oro y electrodomésticos. Nuestro personal altamente calificado de tasadores se encuentra a su dispoción en nuestras más de 80 agencias en todo el país. 
        Contamos con el apoyo de Hermes, como socio estratégico, por lo que brindamos no solo seguridad sino también una buena atención con nuestros procesos centrados en el cliente. " />
  </Head>
  <Component {...pageProps} />
  </>
  )
}

export default MyApp
