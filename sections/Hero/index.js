import styles from './Hero.module.css' 
import Button from '../../components/Button'
import ReactHtmlParser from 'react-html-parser'
import { cleanText } from '../../config/tools/utils'

export default function Hero({ presentation_img_url, presentation_logo_img_url, slogan, presentation_text }) {
    return (
        <section id="home" className={styles.Hero} >
          <div className={styles['Hero__container']}>
            <div className={styles['Hero__images']} >
              <div >
                <div className={styles['Hero__image']}>
                  <img width="350px" height="436px" src={presentation_img_url} alt="trabajador" />
                </div>
                <div className={styles['Hero__logo_text']} >
                  <img width="200px" height="67px" src={presentation_logo_img_url} alt="logo" />
                  <p>{slogan}</p>
                </div>
              </div>
            </div>
            <div className={styles['Hero__information']}>
              <div  className={styles['Hero__information_logo_text']}>
                <img width="467px" height="155px" src={presentation_logo_img_url} alt="logo" />
                <p>{slogan}</p>
              </div>
              <div className={styles['Hero__information_text']}>
                {
                  presentation_text.map((data) => 
                    ReactHtmlParser(cleanText(data.text))
                  )
                }
                <div className={styles['Hero__wrapper_button']}>
                   <a href="#contact"><Button  minimize={false}>CONTÁCTANOS</Button> </a> 
                </div>
              </div>
            </div>
          </div>
      </section>
    )
}
