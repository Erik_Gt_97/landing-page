import { FlexCenter } from '../../config/styles/flex'
import styles from './Steps.module.css'

export default function Steps(){
    return (
        <section className={styles.Steps}>
            <div >
                <FlexCenter>
                <p>PASOS PARA UNA TASACIÓN Y CUSTODIA</p>
                </FlexCenter>
                <div className={styles['Steps__container']}>
                <div className={styles['Step__container']}>
                    <div className={styles['Step__content']} > 
                        <div>
                        <div>
                            <img src="/Step_1.png" alt="paso 1"/>
                        </div>
                        <div className={styles['Step__content_information']} ><p>Acércate a nuestra
                                oficina más cercana.</p></div>
                        </div> 
                        
                    </div>
                    <div className={styles['Step__icon']}>
                        <div>
                        <i className="fa fa-caret-down"></i>
                        </div>
                    </div>
                </div>
                <div className={styles['Step__container']}>
                    <div className={styles['Step__content']} >
                        <div>
                        <div>
                            <img src="/Step_2.png" alt="paso 2"/>
                        </div>
                        <div className={styles['Step__content_information']} ><p>Trae tu joya de oro o un 
                            artículo en buen estado
                            para tasar y custodiar 
                            con tu DNI.</p>
                        </div>
                            </div>   
                    </div>
                    <div className={styles['Step__icon']}>
                        <div>
                            <i className="fa fa-caret-down"></i>
                        </div>
                    </div>
                </div>
                <div className={styles['Step__container']}>
                    <div className={styles['Step__content']} >
                        <div>
                        <div>
                            <img src="/Step_3.png" alt="paso 3"/>
                        </div>
                        <div className={styles['Step__content_information']} ><p>Conoce el valor de la
                                tasación de tus joyas o
                                artículos y guárdalas con
                                nosotros por el tiempo
                                que necesites.</p>
                        </div>
                            </div>  
                        
                    </div>
                    <div className={styles['Step__icon']}>
                        <div>
                            <i className="fa fa-caret-down"></i>
                        </div>
                    </div>
                </div>
                </div>
                
            </div>
        </section>
    )
}