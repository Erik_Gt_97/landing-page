import { useState } from 'react'
import Button from '../../components/Button'
import InputField from '../../components/InputField'
import NumeralField from '../../components/NumeralField'
import SelectField, {Option} from '../../components/SelectField'
import TextAreaField from '../../components/TextAreaField'
import { FlexCenter } from '../../config/styles/flex'
import departments from './departments.json'
import styles from './Contact.module.css'
import { emailIsValid, encode } from '../../config/tools/utils'
import Alert from '../../components/Alert'

export default function Contact( { contact_img_url }) {
    const [disabledButton, setDisabledButton] = useState(false)
    const [alert, setAlert] = useState({
        message: '',
        type: 'danger',
        show: false
    })
    const [form, setForm] = useState({
        nombresApellidos: '',
        celular: '',
        correo: '',
        asunto: '',
        consulta: ''
    })
    const [department, setDepartment] = useState(false)
    const [helper, setHelper] = useState({
        nombresApellidos: '',
        celular: '',
        correo: '',
        departamento: ''
    })
    

    function handleChangeSelect (option){
        if(alert.show){
            setAlert({
                ...alert,
                show: false
            })
        }
        if(disabledButton){
            setDisabledButton(false)
        }
        setDepartment(option)
        setHelper({
            ...helper,
            departamento: ''
        })
        setForm({
            ...form,
            departamento: option.label
        })
    }

    function handleChange (e){
        if(alert.show){
            setAlert({
                ...alert,
                show: false
            })
        }
        if(disabledButton){
            setDisabledButton(false)
        }
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
        if(e.target.value === ''){
            setHelper({
                ...helper,
                [e.target.name]: 'Campo requerido'
            })
        }else{
            setHelper({
                ...helper,
                [e.target.name]: ''
            })
        }
        
    }

    function handleSubmit (e) {
        e.preventDefault();
        if (form.celular === '' || form.celular.length !== 9 || form.nombresApellidos === '' || form.correo ==='' || !department){
            setHelper({
                ...helper,
                ...(form.correo === '' && {
                    correo: 'Campo requerido'
                }),
                ...(form.correo !== '' && !emailIsValid(form.correo) && {
                    correo: 'Formato inválido'
                }),
                ...(form.nombresApellidos === '' && {
                    nombresApellidos: 'Campo requerido'
                }),
                ...(form.celular === '' && {
                    celular: 'Campo requerido'
                }),
                ...(form.celular.length !== 9 && {
                    celular: 'Requiere 9 dígitos'
                }),
                ...(!department && {
                    departamento: 'Campo requerido'
                })
            })
            return 
        }
        fetch("/", {
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          body: encode({ "form-name": "fornulario-contacto", ...form })
        })
          .then(() => { setAlert({
              ...alert,
              message: 'Se envió la consulta exitosamente',
              type: 'success',
              show: true
          })
          setDisabledButton(true)
          })
          .catch(() => setAlert({
              ...alert,
              message: 'Ocurrió un error. Volver a intentar',
              show: true
          }) );
  
        
      };

    return (
        <section id='contact' className={styles.Contact}>
            <div className={styles['Contact__container']}>
                <div >
                    <div className={styles['Contact__container__form']}>
                        <div className={styles.wrapper}>
                    <div className={styles['Contact__form__top']}>
                        <p>¿En qué te podemos ayudar?</p>
                        <p>Contáctanos para brindarte la asesoría más adecuada para tu tasación</p>
                    </div>
                    <form method="post" data-netlify="true" data-netlify-honeypot="bot-field" name="contact-form" onSubmit={handleSubmit} className={styles['Contact__form__content']}  >
                        <input type="hidden" name="form-name" value="formulario-contacto" />
                        <div >
                        <label>Nombres y Apellidos*</label>
                        <InputField placeholder='p. ej., Juan Portella' className='mt-1 mb-3' name="nombresApellidos" helper={helper.nombresApellidos} value={form.nombresApellidos} onChange={handleChange} fullWidth/>
                        <div className={styles['Contact__fields_row']}>
                            <div>
                                <label>Teléfono*</label>
                                <NumeralField name='celular' helper={helper.celular} value={form.celular} onChange={handleChange} placeholder='p. ej., 999 333 555' className='mt-1 mb-2' fullWidth/>
                            </div>
                            <div>
                            <label>Correo*</label>
                                <InputField name="correo" onChange={handleChange} value={form.correo} helper={helper.correo} placeholder='p. ej., nombre@gmail.com' className='mt-1 mb-2' fullWidth/>
                            </div>
                        </div>
                        <div className={styles['Contact__fields_row']}>
                            <div>
                                <label>Departamento*</label>
                                <SelectField helper={helper.departamento} option={department} onChange={handleChangeSelect} placeholder='Seleccione su localidad' className='mt-1 mb-2' fullWidth>
                                    {
                                        departments.map((d,i) => 
                                        <Option key={i} value={d.value}>{d.label}</Option>
                                        )
                                    }
                                </SelectField>
                            </div>
                            <div>
                            <label>Asunto</label>
                                <InputField onChange={handleChange} name="asunto" placeholder='p. ej., Tasación de mi televisor' value={form.asunto} className='mt-1 mb-2' fullWidth/>
                            </div>
                        </div>
                        <label>Consulta</label>
                        <TextAreaField name="consulta" onChange={handleChange} placeholder='Cuéntanos sobre tu equipo y bríndanos algunos detalles sobre la tasación que estás buscando' value={form.consulta} className='mt-1 mb-1' fullWidth/>
                        <Alert type={alert.type} className='mb-2' show={alert.show}>
                            {alert.message}
                        </Alert>
                        <FlexCenter>
                            <Button className='mb-2' disabled={disabledButton} type='submit' style={{background:'var(--moon-yellow)',color:'white',height:'50px',width:'250px',boxShadow: '1px 4px 20px rgba(128, 128, 128, 0.1)'}}>ENVIAR CONSULTA</Button>
                        </FlexCenter>
                        </div>
                    </form>
                    </div>
                    </div>
                    
                </div>
                <div className={styles['Contact__image']}>
                    <img height="400px" width="420px"  src={contact_img_url} alt="Contacto"></img>
                </div>
            </div>
        </section>
    )
}
