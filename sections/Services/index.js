import Card from '../../components/Card'
import { FlexCenter } from '../../config/styles/flex'
import styles from './Services.module.css'

export default function Services({ services }) {
    return (
        <section id="services" className={styles.Services}>
            <div className={styles['Services__container']}>
                <FlexCenter>
                <div>
                <FlexCenter>
                <p className={styles.title}>NUESTROS SERVICIOS</p>
                </FlexCenter>
                <FlexCenter>
                </FlexCenter>
                <div style={{padding:'0px 2.5em'}}>
                    <FlexCenter>
                    <div className={styles['Cards__container']}> 
                     {
                         services.map(service => 
                          <Card key={service.id} image_url={service.image_url} name={service.image_alt} >
                              {service.description}
                          </Card>
                            )
                     }
                    </div>
                    </FlexCenter>
                    <div className={styles['Services__footer']}> 
                        <p>No realizamos tasación de joyas de oro blanco, verde, plata u otros metales preciosos.
                        Asimismo, las joyas y artículos deben encontrarse en buen estado.</p>
                    </div>
                </div>
                </div>             
                </FlexCenter>
            </div>
        </section>
    )
}
