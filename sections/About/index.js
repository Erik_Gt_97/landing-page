import styles from './About.module.css'
import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { cleanText } from '../../config/tools/utils'

export default function About({ mision, mision_img_url, vision, vision_img_url ,about , about_img_url }) {   

    return (
        <section id="about" className={styles.About}>
            <div className={styles['About__container']}>
                <div className={styles['flex_center']}>
                    <img width="460px" height="420px" src={about_img_url} alt="Contacto"></img>
                </div>
                <div className={styles['flex_center']} style={{alignItems:'start'}}>
                    <div className={styles['About__information']}>
                    <p >SOBRE NOSOTROS</p>
                    {
                        about.map((data) => 
                         ReactHtmlParser(cleanText(data.text))
                        )
                    }
                    </div>
                </div>
                <div className={styles['flex_center']}>
                    <div className={styles['left__card']} >
                        <img height="100px" width="100px"  src={mision_img_url} alt="Mision"></img>  
                    </div>
                    <div className={styles['right__card']} >
                        <p>
                            Misión
                        </p>
                        <p>
                            {mision}
                        </p>
                    </div>
                </div>
                <div className={styles['flex_center']}>
                    <div className={styles['left__card']} >
                        <img height="100px" width="100px"   src={vision_img_url} alt="Vision"></img>  
                    </div>
                    <div className={styles['right__card']} >
                        <p>
                            Visión
                        </p>
                        <p>
                        {vision}
                        </p>
                    </div>
                </div>
            </div>
        </section>
    )
}
