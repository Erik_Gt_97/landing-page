import { useState } from 'react'
import List, { ListItem } from '../../components/List'
import MapView, {  MarkerPoint } from '../../components/MapView'
import SearchField from '../../components/SearchField'
import styles from './Ubication.module.css'

export default function Ubication( { google_api_key, offices, markers }) {
    const [listOffices] = useState(offices)
    const [searchOffices, setSearchListOffices] = useState(offices)
    const [text, setText] = useState('')
    const [position,setPosition] = useState({lat: -9.189967, lng:-75.015152})
    const [zoom,setZoom] = useState(5)
    const [value,setValue] = useState(null)

    function handleChange (e){
        setText(e.target.value)
        const list = []
        listOffices.forEach( office => {
            if( office.nombre.toLowerCase().includes(e.target.value.toLowerCase())) {
                list.push(office)
            }
        })
        setSearchListOffices(list)
    }

    function handleClear (){
        setText('')
        setValue(null)
        setSearchListOffices(listOffices)
        setPosition({
            lat: -9.189967,
            lng: -75.015152
        })
        setZoom(5)
    }

    function handleTab(v){
        setValue(v)
    }

    function handleClick(o){
        setPosition({
            lat: o.latitud,
            lng: o.longitud
        })
        setZoom(18)
    }

    return (
        <section id="ubication" className={styles.Ubication}>
            <MapView height='100%' zoom={zoom} onLoad={()=>{}} center={position}  google_api_key={google_api_key}>
                {
                    markers.map((marker,i)=> 
                    <MarkerPoint key={i} position={marker} icon={"https://drive.google.com/uc?export=view&id=1yNyg5oFoEGxbIdUePg-KrjkX7kGQwJ_k"} ></MarkerPoint>
                     )
                }   
            </MapView>
            <div className={styles['List__container']} >
                <div className={styles['List__offices_container']} >
                    <div style={{padding:'20px'}}>
                    <b>Estamos en todo el Perú</b>
                    <SearchField placeholder='Escribe tu ubicación' value={text} onClear={handleClear} onChange={handleChange} fullWidth className='mt-3'></SearchField>
                    </div>
                        <List value={value} onTab={handleTab}>
                            {searchOffices.map((office,i) =>
                               <ListItem click={() => handleClick(office)} value={office.id} key={office.id}>
                                    <b>{office.nombre} </b>
                                    <div style={{display:'flex', alignItems:'center'}}><i className='fa fa-map-marker'></i><p>{office.direccion}</p></div>
                              
                               </ListItem> 
                                )}
                            {searchOffices.length === 0 && 
                            <b style={{fontSize:'14px'}} className='ml-1'>No se encontraron resultados</b>
                            }
                        </List>
                </div>
                <div>
                    
                </div>
            </div>
        </section>
    )
}

